const path = require('path');

module.exports = {
  paths: {
    app: {
      client: path.resolve(__dirname, '../src/client'),
      server: path.resolve(__dirname, '../src/server'),
      build: path.resolve(__dirname, '../build')
    },
    static: path.resolve(__dirname, '../static'),
    logs: path.join(__dirname, '../logs')
  }
};
