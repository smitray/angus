// eslint-disable-next-line import/no-extraneous-dependencies
import { resolve, join } from 'path';
import PurgeCssPlugin from 'purgecss-webpack-plugin';
import glob from 'glob-all';
import OptimizeCssnanoPlugin from '@intervolga/optimize-cssnano-plugin';
import { paths } from './config';

class TailwindExtractor {
  static extract(content) {
    // eslint-disable-next-line no-useless-escape
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
  }
}

export default {
  mode: 'universal',
  srcDir: paths.app.client,
  buildDir: `${paths.app.build}/client`,
  rootDir: './',
  modern: 'server',
  head: {
    title: 'Welcome to',
    titleTemplate: (titleChunk) => (titleChunk ? `${titleChunk} - Angus` : 'Angus')
  },
  css: [
    '@/assets/styles/main.css'
  ],
  loading: {
    color: '#3B8070'
  },

  // Modules and configuration
  modules: [
    // '@nuxtjs/apollo',
    // '@nuxtjs/axios', use only if needed
    'styled-vue/nuxt',
    '@nuxtjs/pwa'
  ],

  // Apollo configuration
  // apollo: {
  //   clientConfigs: {
  //     default: '@/graphql/config'
  //   }
  // },

  // PWA configuration
  meta: {
    description: 'Nuxt and GraphQL boilerplate',
    author: 'Smit Ray <humancoder@outlook.com, hi@smitray.me, smit@codends.net>'
  },

  // Custom build configuration
  build: {
    extractCSS: true,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    },
    postcss: {
      plugins: {
        tailwindcss: resolve(__dirname, './tailwind.config.js'),
        'postcss-partial-import': {},
        'postcss-crip': {},
        'postcss-mixins': {},
        'postcss-advanced-variables': {},
        'postcss-short': {},
        'postcss-nested': {},
        'postcss-ref': {},
        'postcss-property-lookup': {},
        'postcss-utilities': {},
        'rucksack-css': {},
        'postcss-extend': {},
        'css-mqpacker': {},
        'postcss-media-minmax': {},
        'postcss-merge-rules': {}
      },
      preset: {
        stage: 0,
        autoprefixer: {
          cascade: false,
          grid: true
        },
        features: {
          'nesting-rules': false
        }
      }
    },
    extend(config, { isDev }) {
      // config.node = {
      //   fs: 'empty'
      // };
      if (!isDev) {
        config.plugins.push(
          new PurgeCssPlugin({
            paths: glob.sync([
              join(__dirname, './src/client/pages/**/*.vue'),
              join(__dirname, './src/client/layouts/**/*.vue'),
              join(__dirname, './src/client/components/**/*.vue')
            ]),
            extractors: [
              {
                extractor: TailwindExtractor,
                extensions: ['vue', 'js', 'html']
              }
            ],
            whitelist: ['html', 'body', 'nuxt-progress']
          })
        );
        config.plugins.push(
          new OptimizeCssnanoPlugin({
            cssnanoOptions: {
              preset: ['default', {
                discardComments: {
                  removeAll: true
                },
                zIndex: false
              }]
            }
          })
        );
      }
    }
  }
};
